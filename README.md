# Informations
Sont présents ici 3 noeuds principaux

# Sous-noeud de decision
# Noeud Rotation
Envoi de commandes au moteur afin d'effectuer des rotations

# Noeud Translation
Envoi de commandes au moteur afin d'effectuer des translations

# Noeud d'affichage
# Noeud usb_cam
Attention, ce noeud a besoin du noeud image_view afin d'afficher ce que voit la caméra
